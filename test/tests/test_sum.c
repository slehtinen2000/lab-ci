#include "sum.h"
#include "unity.h"
#include "limits.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_sum_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_sum(0, 0), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_sum(0, 1), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_sum(1, 0), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_sum(-1, 1), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-1, integer_sum(-1, 0), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-2, integer_sum(-1, -1), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(25, integer_sum(10, 15), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-50, integer_sum(100, -150), "Error in integer_sum");

    //val max + 1 should be possible but gives min value  
    TEST_ASSERT_EQUAL_INT_MESSAGE(INT_MIN, integer_sum(INT_MAX, 1), "Error in integer_sum");
    //val max + 2 should be possible but gives min value + 1  
    TEST_ASSERT_EQUAL_INT_MESSAGE((INT_MIN+1), integer_sum(INT_MAX, 2), "Error in integer_sum");

    //val min - 1 should be possible but gives max value  
    TEST_ASSERT_EQUAL_INT_MESSAGE(INT_MAX, integer_sum(INT_MIN, -1), "Error in integer_sum");
    //val min - 2 should be possible but gives max value -1  
    TEST_ASSERT_EQUAL_INT_MESSAGE((INT_MAX-1), integer_sum(-2147483648, -2), "Error in integer_sum");
    
}

