#include "avg.h"
#include "unity.h"
#include "limits.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_avg_function(void)
{
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_avg(1, 1), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(5, integer_avg(10, 0), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(50, integer_avg(96, 4), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(13, integer_avg(26, 0), "Error in integer_avg");

}